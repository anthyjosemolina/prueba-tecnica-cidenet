-- 1. Se crea la tabla paises y su secuencia

-- 1.1. creamos la secuencia
create sequence cd_paises_sq;

-- 1.2. creamos la tabla
create table cd_paises(
    id_pais     number          default     cd_paises_sq.nextval 
                                constraint  cd_paises_pais_id_pk        primary key
                                constraint  cd_paises_pais_id_nn        not null,
    nombre_pais varchar2(50)    constraint  cd_paises_nombre_pais_un    unique
                                constraint  cd_paises_nombre_pais_nn    not null
);
comment on table    cd_paises               is 'Entidad donde se Almacenan los Paises del Sistema';
comment on column   cd_paises.id_pais       is 'Identificador unico del pais';
comment on column   cd_paises.nombre_pais   is 'Nombre del Pais';

-- 2. Se crea la tabla identificaiones tipo y su secuencia

-- 2.1. creamos la secuencia
create sequence cd_identificaciones_tipo_sq;

-- 2.2. creamos la tabla
create table cd_identificaciones_tipo(
    id_identificaciones_tipo    number          default     cd_identificaciones_tipo_sq.nextval
                                                constraint  cd_ident_tipo_id_ident_tipo_pk  primary key
                                                constraint  cd_ident_tipo_id_ident_tipo_nn  not null,
    descripcion                 varchar2(50)    constraint  cd_ident_tipo_descripcion_un    unique
                                                constraint  cd_ident_tipo_descripcion_nn    not null
);
comment on table    cd_identificaciones_tipo                            is 'Entidad donde se Almacenan los Tipos de Identificaciones del Sistema';
comment on column   cd_identificaciones_tipo.id_identificaciones_tipo   is 'Identificador unico del tipo de identificacion';
comment on column   cd_identificaciones_tipo.descripcion                is 'Nombre del tipo de identificacion';

-- 3. creamos la tabla areas y su secuencia

-- 3.1 creamos la secuencia
create sequence cd_areas_sq;

-- 3.2 creamos la tabla 
create table cd_areas(
    id_area     number          default     cd_areas_sq.nextval
                                constraint  cd_areas_id_area_pk     primary key
                                constraint  cd_areas_id_area_nn     not null,
    nombre_area varchar2(50)    constraint  cd_areas_nombre_area_un unique
                                constraint  cd_areas_nombre_area_nn not null
);
comment on table    cd_areas                is 'Entidad donde se Almacenan las Areas del Sistema';
comment on column   cd_areas.id_area        is 'Identificador unico del area';
comment on column   cd_areas.nombre_area    is 'Nombre del area';


-- 4. creamos la tabla empleados y su secuencia

-- 4.1. creamos la secuencia
create sequence cd_empleados_sq;

-- 4.2 creamos la tabla
create table cd_empleados(
    id_empleado                 number          default     cd_empleados_sq.nextval
                                                constraint  cd_emp_id_empleado_pk           primary key
                                                constraint  cd_emp_id_empleado_nn           not null,
    primer_nombre               varchar2(20)    constraint  cd_emp_primer_nombre_nn         not null
                                                constraint  cd_emp_primer_nombre_ck         check           (regexp_like(primer_nombre,'^[[:upper:]][^ñÑáéíóúüÁÉÍÓÚÜ ]+$')),
    otros_nombres               VARCHAR2(50)    CONSTRAINT  cd_emp_otros_nombres_ck         check           (regexp_like(otros_nombres,'^[[:upper:]][^ñÑáéíóúüÁÉÍÓÚÜ]+$')),
    primer_apellido             varchar2(20)    constraint  cd_emp_primer_apellido_nn       not null    
                                                constraint  cd_emp_primer_apellido_ck       check           (regexp_like(primer_apellido,'^[[:upper:]][^ñÑáéíóúüÁÉÍÓÚÜ]+$')),
    segundo_apellido            varchar2(20)    constraint  cd_emp_segundo_apellido_nn      not null    
                                                constraint  cd_emp_segundo_apellido_ck      check           (regexp_like(segundo_apellido,'^[[:upper:]][^ñÑáéíóúüÁÉÍÓÚÜ]+$')),
    id_pais                     number          constraint  cd_emp_id_pais_fk               references      cd_paises(id_pais)
                                                constraint  cd_emp_id_pais_nn               not null,
    id_identificaciones_tipo    number          constraint  cd_emp_id_ident_tipo_fk         references      cd_identificaciones_tipo(id_identificaciones_tipo)
                                                constraint  cd_emp_id_ident_tipo_nn         not null,
    numero_identificacion       varchar2(20)    constraint  cd_empl_num_identificacion_nn   not null
                                                constraint  cd_empl_num_identificacion_ck   check           (regexp_like(numero_identificacion,'^([A-Za-z0-9]+[A-Za-z0-9-]+[A-Za-z0-9]+)$')),
    correo                      varchar2(300)   constraint  cd_emp_correo_nn                not null,
    fecha_ingreso               date            constraint  cd_emp_fecha_ingreso_nn         not null,
    id_area                     number          constraint  cd_emp_id_area_fk               references      cd_areas(id_area)
                                                constraint  cd_emp_id_area_nn               not null,
    estado                      number          default     1
                                                constraint  cd_emp_estado_ck                check           (estado in(1,0))
                                                constraint  cd_emp_estado_nn                not null,
    fecha_registro              timestamp(6)    default     sysdate
                                                constraint  cd_emp_fecha_registro_nn        not null,
    fecha_edicion               timestamp(6),

                                                constraint  cd_emp_id_idnt_tip_num_idnt_un  unique          (id_identificaciones_tipo, numero_identificacion)                                            
);
comment on table    cd_empleados                            is 'Entidad donde se almacenan los empleados del sistema';
comment on column   cd_empleados.id_empleado                is 'Identificador unico del empleado';
comment on column   cd_empleados.primer_nombre              is 'Primer nombre del empleado';
comment on column   cd_empleados.otros_nombres              is 'Otros nombres nombre del empleado';
comment on column   cd_empleados.primer_apellido            is 'Primer apellido nombre del empleado';
comment on column   cd_empleados.segundo_apellido           is 'Segundo apellido nombre del empleado';
comment on column   cd_empleados.id_pais                    is 'identificador del pais del empleado';
comment on column   cd_empleados.id_identificaciones_tipo   is 'identificador del tipo de identificacion del empleado';
comment on column   cd_empleados.numero_identificacion      is 'Numero de identificacion del empleado';
comment on column   cd_empleados.correo                     is 'Correo del empleado';
comment on column   cd_empleados.fecha_ingreso              is 'Fecha ingreso del empleado';
comment on column   cd_empleados.fecha_edicion              is 'Fecha edicion del empleado';
comment on column   cd_empleados.id_area                    is 'Identificador del area del empleado';
comment on column   cd_empleados.estado                     is 'Estado del empleado 1 = Activo y 0 = Inactivo por defecto sera = 1';
--check           ((fecha_ingreso >= (trunc(sysdate) - interval '1' month)) and fecha_ingreso <= trunc(sysdate)),

-- 5. Creamos la tabla dominio y su secuencia

-- 5.1 creamos la secuencia
create sequence cd_dominios_sq;

-- 5.2 creamos la tabla
create table cd_dominios(
	id_dominio  number          default cd_dominios_sq.nextval      
                                constraint cd_dominios_id_dominio_pk    primary key 
                                constraint cd_dominios_id_dominio_nn    not null,
    dominio     varchar2(50)    constraint cd_dominios_dominio_nn       not null,
    id_pais     number          constraint cd_dominios_id_pais_fk       references cd_paises(id_pais)
                                constraint cd_dominios_id_pais_nn       not null
);
comment on table    cd_dominios             is 'Entidad donde se almacenan los dominios del sistema';
comment on column   cd_dominios.id_dominio  is 'Identificador unico del dominio';
comment on column   cd_dominios.dominio     is 'dominio del dominio';
comment on column   cd_dominios.id_pais     is 'identificacion del pais al cual pertenece el dominio';
