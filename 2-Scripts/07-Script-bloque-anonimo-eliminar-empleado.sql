-- Bloque anonimo que elimina un empleado dado un id
set serveroutput on;
declare
    v_codigo    number;
    v_mensaje   varchar2(4000);
begin
    cd_registro_empleados.cd_prc_eliminar_empleado(
         i_id_empleado => 1
       , o_codigo      => v_codigo
       , o_mensaje     => v_mensaje
    );
    dbms_output.put_line(v_mensaje);
end;
/