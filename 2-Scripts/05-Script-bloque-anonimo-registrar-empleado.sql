-- Bloque anonimo para registrar empleado
set serveroutput on;
declare
	i_id_empleado                 varchar2(200);
	i_primer_nombre               varchar2(200);
	i_otros_nombres               varchar2(200);
	i_primer_apellido             varchar2(200);
	i_segundo_apellido            varchar2(200);
	i_id_pais                     number;
	i_id_identificaciones_tipo    number;
	i_numero_identificacion       varchar2(200);
	i_id_area                     number;
	i_fecha_ingreso               date;
	i_accion                      varchar2(200);
	o_codigo                      number;
	o_mensaje                     varchar2(200);
begin
	i_primer_nombre               := 'ANTONIO';
	i_otros_nombres               := 'JOSE';
	i_primer_apellido             := 'MOLINA';
	i_segundo_apellido            := 'MOLINA';
	i_id_pais                     := 1;
	i_id_identificaciones_tipo    := 1;
	i_numero_identificacion       := '1044394085';
	i_id_area                     := 1;
	i_fecha_ingreso               := sysdate;

	cd_registro_empleados.cd_prc_registrar_empleado(
		i_primer_nombre             => i_primer_nombre,
		i_otros_nombres             => i_otros_nombres,
		i_primer_apellido           => i_primer_apellido,
		i_segundo_apellido          => i_segundo_apellido,
		i_id_pais                   => i_id_pais,
		i_id_identificaciones_tipo  => i_id_identificaciones_tipo,
		i_numero_identificacion     => i_numero_identificacion,
		i_id_area                   => i_id_area,
		i_fecha_ingreso             => i_fecha_ingreso,
		o_codigo                    => o_codigo,
		o_mensaje                   => o_mensaje
	);
  
	dbms_output.put_line('o_mensaje = ' || o_mensaje);
end;
/