--1. Creamos la vista de la tabla de empleados
create or replace view v_cd_empleados as
select b.descripcion as tipo_identificacion
     , b.id_identificaciones_tipo
     , a.numero_identificacion 
     , a.primer_nombre
     , a.otros_nombres
     , a.primer_apellido
     , a.segundo_apellido
     , a.correo
     , c.id_area
     , c.nombre_area
     , d.id_pais
     , d.nombre_pais
     , a.fecha_ingreso
     , a.fecha_registro
     , a.fecha_edicion
     , a.estado
     , decode(a.estado, 1, 'Activo', 0, 'Inactivo') as descripcion_estado          
  from cd_empleados             a
  join cd_identificaciones_tipo b on a.id_identificaciones_tipo = b.id_identificaciones_tipo
  join cd_areas                 c on a.id_area                  = c.id_area
  join cd_paises                d on a.id_pais                  = d.id_pais;