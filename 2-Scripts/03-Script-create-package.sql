create or replace package cd_registro_empleados as 
    /*
        Autor: Antonio Molina Molina
        Descripcion: Paquete que contiene las unidades de programa necesarias para llevar acabo el registro, edicion y consulta de empleados
    */
    -- Codigo: prc_1
    -- Descripcion: Procedimiento que realiza crud de la tabla cd_empleados
    procedure cd_prc_registrar_empleado(
         i_primer_nombre             in  varchar2
       , i_otros_nombres             in  varchar2
       , i_primer_apellido           in  varchar2
       , i_segundo_apellido          in  varchar2
       , i_id_pais                   in  number
       , i_id_identificaciones_tipo  in  number
       , i_numero_identificacion     in  varchar2
       , i_id_area                   in  number
       , i_fecha_ingreso             in  date       
       , o_codigo                    out number
       , o_mensaje                   out varchar2
    );
    
    -- Codigo: prc_2
    -- Descripcion: procedimiento que actualiza un empleado dado el id
    procedure cd_prc_actualizar_empleado(
          i_id_empleado               in number
        , i_primer_nombre             in varchar2
        , i_otros_nombres             in varchar2
        , i_primer_apellido           in varchar2
        , i_segundo_apellido          in varchar2
        , i_id_pais                   in number
        , i_id_identificaciones_tipo  in number
        , i_numero_identificacion     in varchar2
        , i_id_area                   in number
        , o_codigo                    out number  
        , o_mensaje                   out varchar2        
    );
    
    -- Codigo: prc_3
    -- Descripcion: procedimiento que elimina el empleado
    procedure cd_prc_eliminar_empleado(
         i_id_empleado               in  varchar2
       , o_codigo                    out number
       , o_mensaje                   out varchar2        
    );    
    
    
    -- Codigo: fnc_1
    -- Descripcion: Funcion que genera el correo del empleado
    function cd_fnc_generar_correo(
         i_primer_nombre     varchar2
       , i_primer_apellido   varchar2
       , i_id_pais           number    
    )return varchar2;
    
    -- Codigo: fnc_2
    -- Descripcion: Funcion que consulta empleados dados ciertos parametros
    function cd_fnc_consultar_empleado(
        i_id_identificaciones_tipo  number
      , i_numero_identificacion     varchar2
      , i_primer_nombre             varchar2
      , i_otros_nombres             varchar2
      , i_primer_apellido           varchar2
      , i_segundo_apellido          varchar2
      , i_id_pais                   number
      , i_correo                    varchar2
      , i_estado                    number
    )
    return sys_refcursor;
end cd_registro_empleados;
/
create or replace package body cd_registro_empleados as
    /*
        Autor: Antonio Molina Molina
        Descripcion: Paquete que contiene las unidades de programa necesarias para llevar acabo el registro, edicion y consulta de empleados
    */
    -- Codigo: prc_1
    -- Descripcion: Procedimiento que realiza crud de la tabla cd_empleados
    procedure cd_prc_registrar_empleado(
             i_primer_nombre             in  varchar2
           , i_otros_nombres             in  varchar2
           , i_primer_apellido           in  varchar2
           , i_segundo_apellido          in  varchar2
           , i_id_pais                   in  number
           , i_id_identificaciones_tipo  in  number
           , i_numero_identificacion     in  varchar2
           , i_id_area                   in  number
           , i_fecha_ingreso             in  date
           , o_codigo                    out number
           , o_mensaje                   out varchar2
    )
    as
        v_correo                   varchar2(300);
        v_id_identificaciones_tipo varchar2(50);
        -- excepcion check constraint
        check_constraint_violated exception;
        pragma exception_init(check_constraint_violated, -2290);        
    begin
        -- Respuesta Exitosa
        o_codigo  := 0;        
        
        -- validamos la fecha de ingreso
        if not((trunc(i_fecha_ingreso) >= (trunc(sysdate) - 30 /*interval '1' month*/)) and trunc(i_fecha_ingreso) <= trunc(sysdate)) then
            o_codigo  := 100;
            o_mensaje := 'Error prc_1-'||o_codigo||' al registrar empleado: no se pede realizar el registro, la fecha de ingreso no se encuentra dentro del rango admitido.';
            return;                    
        end if;
        -- validamos la fecha de ingreso
        
        -- Generamos el correo
        v_correo := cd_registro_empleados.cd_fnc_generar_correo(i_primer_nombre, i_primer_apellido, i_id_pais);
        
        if v_correo is null then
            o_codigo  := 150;
            o_mensaje := 'Error prc_1-'||o_codigo||' al registrar empleado: Error al generar el coreo del empleado.';
            return;                     
        end if;
        
        -- Registramos el Empleado
        begin
            insert into cd_empleados ( 
                                           primer_nombre 
                                         , otros_nombres
                                         , primer_apellido
                                         , segundo_apellido
                                         , id_pais
                                         , id_identificaciones_tipo
                                         , numero_identificacion
                                         , correo
                                         , fecha_ingreso
                                         , id_area
                                     )
                              values (
                                           i_primer_nombre
                                         , i_otros_nombres
                                         , i_primer_apellido
                                         , i_segundo_apellido
                                         , i_id_pais
                                         , i_id_identificaciones_tipo
                                         , i_numero_identificacion
                                         , v_correo
                                         , i_fecha_ingreso
                                         , i_id_area
                                     ); 
            -- mensaje Exitoso
            o_mensaje := 'Empleado registrado con exito.';                                             
        exception
            -- excepcion que valida unique index tipo de identificacion e identificacion
            when dup_val_on_index then
                select lower(descripcion) into v_id_identificaciones_tipo from cd_identificaciones_tipo where id_identificaciones_tipo = i_id_identificaciones_tipo;
                o_codigo  := 200;
                o_mensaje := 'Error prc_1-'||o_codigo||' al registrar empleado: el empleado con '||v_id_identificaciones_tipo||' N° '||i_numero_identificacion||' ya se encuentra registrado.';
                rollback;
                return;
            -- excepcion que valida que los campos i_primer_nombre, i_otros_nombres, i_primer_apellido, i_segundo_apellido e i_numero_identificacion 
            -- cumplan con la expresion regular del constraint
            when check_constraint_violated then
                o_codigo  := 250;
                o_mensaje := 'Error prc_1-'||o_codigo||' al registrar empleado: no cumple las validaciones necesarias.';
                rollback;
                return;
            -- excepcion que valida cualquier excepcion no controlada 
            when others then
                o_codigo  := 300;
                o_mensaje := 'Error prc_1-'||o_codigo||' al registrar empleado: ocurrio un error inesperado al registrar el empleado. '|| sqlerrm;
                rollback;
                return;                        
        end;
    
    -- Confirmamos los cambios
    commit;
    exception 
        when others then
                o_codigo  := 350;
                o_mensaje := 'Error prc_1-'||o_codigo||' al registrar empleado: ocurrio un error inesperado al registrar el empleado. '|| sqlerrm;
                rollback;
                return;                    
    end cd_prc_registrar_empleado;

    -- Codigo: prc_2
    -- Descripcion: procedimiento que actualiza un empleado dado el id
    procedure cd_prc_actualizar_empleado(
          i_id_empleado               in  number
        , i_primer_nombre             in  varchar2
        , i_otros_nombres             in  varchar2
        , i_primer_apellido           in  varchar2
        , i_segundo_apellido          in  varchar2
        , i_id_pais                   in  number
        , i_id_identificaciones_tipo  in  number
        , i_numero_identificacion     in  varchar2
        , i_id_area                   in  number
        , o_codigo                    out number  
        , o_mensaje                   out varchar2  
    )
    as
        v_primer_nombre     varchar2(20);
        v_primer_apellido   varchar2(20);
        v_id_pais           number;
        v_correo            varchar2(300);
        v_id_identificaciones_tipo varchar2(50);

        -- excepcion check constraint
        check_constraint_violated exception;
        pragma exception_init(check_constraint_violated, -2290);        
    begin
        -- Validamos si se cambio el nombre o el apellido
        begin
            select primer_nombre
                 , primer_apellido
                 , correo
                 , id_pais
              into v_primer_nombre
                 , v_primer_apellido
                 , v_correo
                 , v_id_pais
              from cd_empleados
             where id_empleado = i_id_empleado;      
        exception
            when no_data_found then
                o_codigo  := 100;
                o_mensaje :='Error prc_2-'||o_codigo||' al registrar empleado: No se encontro el empleado a actualizar.';
                return;
        end;
        -- Validamos si cambio el nombre apellido o pais para generar de nuevo el correo
        if (i_primer_nombre != v_primer_nombre or i_primer_apellido != v_primer_apellido or i_id_pais != v_id_pais) then
            -- Generamos el correo
            v_correo := cd_registro_empleados.cd_fnc_generar_correo(i_primer_nombre, i_primer_apellido, i_id_pais);
            
            if v_correo is null then
                o_codigo  := 150;
                o_mensaje :='Error prc_2-'||o_codigo||' al registrar empleado: Error al generar el coreo del empleado.';
                return;                     
            end if;                    
        end if;
         
        -- actualizamos el empleado
        update cd_empleados
           set primer_nombre            = i_primer_nombre
             , otros_nombres            = i_otros_nombres
             , primer_apellido          = i_primer_apellido
             , segundo_apellido         = i_segundo_apellido
             , id_pais                  = i_id_pais
             , id_identificaciones_tipo = i_id_identificaciones_tipo
             , numero_identificacion    = i_numero_identificacion
             , id_area                  = i_id_area
             , fecha_edicion            = sysdate
             , correo                   = v_correo
         where id_empleado = i_id_empleado;   
        
        -- Validamos si actualizo registros
        if sql%rowcount = 0 then
            o_codigo  := 200;
            o_mensaje := 'Error prc_2-'||o_codigo||' al registrar empleado: no se encontro el empleado a actualizar.';
        else
            -- mensaje Exitoso
            o_mensaje := 'Empleado Actualizado con exito.';                    
        end if;      
        
        commit;
    exception
        -- excepcion que valida unique index tipo de identificacion e identificacion
        when dup_val_on_index then
            select lower(descripcion) into v_id_identificaciones_tipo from cd_identificaciones_tipo where id_identificaciones_tipo = i_id_identificaciones_tipo;
            o_codigo  := 250;
            o_mensaje := 'Error prc_2-'||o_codigo||' al registrar empleado: el empleado con '||v_id_identificaciones_tipo||' N° '||i_numero_identificacion||' ya se encuentra registrado.';
            rollback;
            return;
        -- excepcion que valida que los campos i_primer_nombre, i_otros_nombres, i_primer_apellido, i_segundo_apellido e i_numero_identificacion 
        -- cumplan con la expresion regular del constraint
        when check_constraint_violated then
            o_codigo  := 300;
            o_mensaje := 'Error prc_2-'||o_codigo||' al registrar empleado: no cumple las validaciones necesarias.';
            rollback;
            return;                
        when others then
            o_codigo  := 350;
            o_mensaje := 'Error prc_2-'||o_codigo||' al registrar empleado: ocurrio un error inesperado al actualizar el empleado. '|| sqlerrm;
            rollback;
            return;                         
    end cd_prc_actualizar_empleado;

    -- Codigo: prc_3
    -- Descripcion: procedimiento que elimina el empleado
    procedure cd_prc_eliminar_empleado(
         i_id_empleado               in varchar2
       , o_codigo                    out number
       , o_mensaje                   out varchar2        
    )
    as
    begin
    -- Eliminamos el empleado
        delete from cd_empleados
         where id_empleado = i_id_empleado;
        
        -- Validamos si elimino registros
        if sql%rowcount = 0 then
            o_codigo  := 100;
            o_mensaje := 'Error prc_3-'||o_codigo||' al registrar empleado: no se encontro el empleado a eliminar.';
        else
            -- mensaje Exitoso
            o_mensaje := 'Empleado eliminado con exito.';                    
        end if;
        
    exception
        when others then 
            o_codigo  := 150;
            o_mensaje := 'Error prc_3-'||o_codigo||' al registrar empleado: ocurrio un error inesperado al eliminar el empleado. '|| sqlerrm;
            rollback;
            return;                                                
    -- Fin eliminamos el empleado       
    end cd_prc_eliminar_empleado;

    -- Codigo: fnc_1
    -- Descripcion: Funcion que genera el correo del empleado
    function cd_fnc_generar_correo(
             i_primer_nombre     varchar2
           , i_primer_apellido   varchar2
           , i_id_pais           number    
    )
    return varchar2
    as
        v_count     number;
        v_dominio   varchar2(50);
    begin
        -- Consultamos el dominio
        select dominio
          into v_dominio
          from cd_dominios
         where id_pais = i_id_pais;
         
        -- Consultamos si existe un empleado con el mismo nombre y apellido para generar el contador
        select count(*)
          into v_count
          from cd_empleados a
          join cd_paises    b on a.id_pais = b.id_pais
         where a.primer_nombre   = i_primer_nombre
           and a.primer_apellido = trim(i_primer_apellido)
           and a.id_pais         = i_id_pais;
        
        -- Si encontro uno o mas empleados le sumamos 1 al contador y generamos el correo
        if v_count > 0 then
            v_count := v_count + 1;
            return lower(i_primer_nombre ||'.'|| replace(i_primer_apellido, ' ','') || '.' || v_count || '@' || v_dominio);
        -- Si no generamos el correo sin contador
        else
            return lower(i_primer_nombre ||'.'|| replace(i_primer_apellido, ' ','') || '@' ||v_dominio);
        end if;
    exception
        when others then
            return null;
    end cd_fnc_generar_correo;   

    -- Codigo: fnc_2
    -- Descripcion: Funcion que consulta empleados dados ciertos parametros
    function cd_fnc_consultar_empleado(
        i_id_identificaciones_tipo  number
      , i_numero_identificacion     varchar2
      , i_primer_nombre             varchar2
      , i_otros_nombres             varchar2
      , i_primer_apellido           varchar2
      , i_segundo_apellido          varchar2
      , i_id_pais                   number
      , i_correo                    varchar2
      , i_estado                    number
    )
    return sys_refcursor
    as
        c_cursor    sys_refcursor;
    begin
        open c_cursor for
        select *  
          from v_cd_empleados             a
         where a.id_identificaciones_tipo = nvl(i_id_identificaciones_tipo, a.id_identificaciones_tipo)
           and a.numero_identificacion    = nvl(i_numero_identificacion, a.numero_identificacion)
           and a.primer_nombre            = nvl(i_primer_nombre, a.primer_nombre)
           and (i_otros_nombres is null or  a.otros_nombres = i_otros_nombres )
           and a.primer_apellido          = nvl(i_primer_apellido, a.primer_apellido)
           and a.segundo_apellido         = nvl(i_segundo_apellido, a.segundo_apellido)
           and a.id_pais                  = nvl(i_id_pais, a.id_pais)
           and a.correo                   = nvl(i_correo, a.correo) 
           and a.estado                   = nvl(i_estado , a.estado);
    
        return c_cursor;
    exception
        when others then
            return null;
    end;
    
end cd_registro_empleados;