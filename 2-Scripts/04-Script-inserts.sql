-- 1. Insertamos los tipos de identificacion del sistema
insert into cd_identificaciones_tipo (descripcion) values ('CÉDLA DE CIUDADANÍA');
insert into cd_identificaciones_tipo (descripcion) values ('CÉDLA DE EXTRANJERÍA');
insert into cd_identificaciones_tipo (descripcion) values ('PASAPORTE');
insert into cd_identificaciones_tipo (descripcion) values ('PERMISO ESPECIAL');

-- 2. Insertamos los paises del sistema
insert into cd_paises (nombre_pais) values ('COLOMBIA');
insert into cd_paises (nombre_pais) values ('ESTADOS UNIDOS');

-- 3. insertamos los dominios del sistema
insert into cd_dominios (dominio, id_pais) values ('cidenet.com.co', 1);
insert into cd_dominios (dominio, id_pais) values ('cidenet.com.us', 2);

-- 4. Insertamos las areas del sistema
insert into cd_areas (nombre_area) values ('ADMINISTRACIÓN');
insert into cd_areas (nombre_area) values ('FINANCIERA');
insert into cd_areas (nombre_area) values ('COMPRAS');
insert into cd_areas (nombre_area) values ('INFRAESTRUCTURA');
insert into cd_areas (nombre_area) values ('OPERACIÓN');
insert into cd_areas (nombre_area) values ('TALENTO HUMANO');
insert into cd_areas (nombre_area) values ('SERVICIOS VARIOS');

-- Confirmamos los cambios
commit;