-- Bloque anonimo para actualizar un empleado dado un id
set serveroutput on;
declare
    i_id_empleado              number;
    i_primer_nombre            varchar2(200);
    i_otros_nombres            varchar2(200);
    i_primer_apellido          varchar2(200);
    i_segundo_apellido         varchar2(200);
    i_id_pais                  number;
    i_id_identificaciones_tipo number;
    i_numero_identificacion    varchar2(200);
    i_id_area                  number;
    o_codigo                   number;
    o_mensaje                  varchar2(200);
begin
    i_id_empleado              := 1;
    i_primer_nombre            := 'ANTONIO';
    i_otros_nombres            := 'JOSE';
    i_primer_apellido          := 'PADILLA';
    i_segundo_apellido         := 'MOLINA';
    i_id_pais                  := 2;
    i_id_identificaciones_tipo := 1;
    i_numero_identificacion    := '1044394085';
    i_id_area                  := 1;

    cd_registro_empleados.cd_prc_actualizar_empleado(
        i_id_empleado              => i_id_empleado,
        i_primer_nombre            => i_primer_nombre,
        i_otros_nombres            => i_otros_nombres,
        i_primer_apellido          => i_primer_apellido,
        i_segundo_apellido         => i_segundo_apellido,
        i_id_pais                  => i_id_pais,
        i_id_identificaciones_tipo => i_id_identificaciones_tipo,
        i_numero_identificacion    => i_numero_identificacion,
        i_id_area                  => i_id_area,
        o_codigo                   => o_codigo,
        o_mensaje                  => o_mensaje
    );

    dbms_output.put_line('o_mensaje = ' || o_mensaje);
end;
/

-- 3. Bloque anonimo que elimina un empleado dado un id
set serveroutput on;
declare
    v_codigo    number;
    v_mensaje   varchar2(4000);
begin
    cd_registro_empleados.cd_prc_eliminar_empleado(
         i_id_empleado => 1
       , o_codigo      => v_codigo
       , o_mensaje     => v_mensaje
    );
    dbms_output.put_line(v_mensaje);
end;
/

-- 4. Bloque anonimo que consulta los empleados del sistema dados ciertos parametros
set serveroutput on;
declare
    -- Cursor
    c_cursor sys_refcursor;
    
    -- Registro 
    r_v_cd_empleados    v_cd_empleados%rowtype;    
    
    -- parametros de consulta
    v_id_identificaciones_tipo  number          := null;
    v_numero_identificacion     varchar2(20)    := null;
    v_primer_nombre             varchar2(20)    := null;
    v_otros_nombres             varchar2(50)    := null;
    v_primer_apellido           varchar2(20)    := null;
    v_segundo_apellido          varchar2(20)    := null;
    v_id_pais                   number          := null;
    v_correo                    varchar2(300)   := null;
    v_estado                    number          := null;
begin
    c_cursor := cd_registro_empleados.cd_fnc_consultar_empleado(
                    i_id_identificaciones_tipo  => v_id_identificaciones_tipo
                  , i_numero_identificacion     => v_numero_identificacion
                  , i_primer_nombre             => v_primer_nombre
                  , i_otros_nombres             => v_otros_nombres
                  , i_primer_apellido           => v_primer_apellido
                  , i_segundo_apellido          => v_segundo_apellido
                  , i_id_pais                   => v_id_pais
                  , i_correo                    => v_correo
                  , i_estado                    => v_estado
                );
    loop
        fetch c_cursor into r_v_cd_empleados;
        exit when c_cursor%notfound;
        dbms_output.put_line('-------------------------------------------------------------------------------------------');
        dbms_output.put_line('Tipo de Identificación: '||r_v_cd_empleados.tipo_identificacion||', N°: '||r_v_cd_empleados.numero_identificacion);
        dbms_output.put_line('Nombre: '||r_v_cd_empleados.primer_nombre||' '||r_v_cd_empleados.otros_nombres||' '||r_v_cd_empleados.primer_apellido||' '||r_v_cd_empleados.segundo_apellido);
        dbms_output.put_line('Correo: '||r_v_cd_empleados.correo);
        dbms_output.put_line('Area: '||r_v_cd_empleados.nombre_area);
        dbms_output.put_line('Pais: '||r_v_cd_empleados.nombre_pais);
        dbms_output.put_line('Fecha de Ingreso: '||r_v_cd_empleados.fecha_ingreso);
        dbms_output.put_line('Fecha de registro: '||r_v_cd_empleados.fecha_registro);
        dbms_output.put_line('Fecha de edición: '||r_v_cd_empleados.fecha_edicion);
        dbms_output.put_line('Estado: '||r_v_cd_empleados.descripcion_estado);
    end loop;
end;
/